class AssignedRole < ActiveRecord::Base
  attr_accessible :role_id, :roleable_id, :roleable_type, :role, :roleable
  belongs_to :role
  belongs_to :roleable, polymorphic: true
  validates :role_id, :roleable_id, :roleable_type, presence: true

end

# == Schema Information
#
# Table name: assigned_roles
#
#  id            :integer          not null, primary key
#  role_id       :integer
#  roleable_id   :integer
#  roleable_type :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

