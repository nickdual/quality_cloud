class Group < ActiveRecord::Base
  attr_accessible :name, :role_ids, :creator
  attr_accessor :creator, :new_group_id

  has_many :assigned_roles, dependent: :destroy, as: :roleable
  has_many :roles, through: :assigned_roles, source: :role
  has_many :users

  validates :name, :presence => true
  validates :name, :uniqueness => {:case_sensitive => false}
  validate :must_have_role

  default_scope order('groups.name ASC')

  after_commit :give_user_access_to_group
  before_destroy :check_preset_groups
  after_destroy :assign_users_to_new_group
  before_save :check_preset_groups_attributes

  def assign_users_to_new_group
    self.users.update_all :group_id => self.new_group_id
  end

  def must_have_role
    if roles.empty? or roles.all? {|role| role.marked_for_destruction? }
      errors.add(:groups, 'must have at least one role')
    end
  end

  def has_role?
    ! (roles.empty? or roles.all? {|role| role.marked_for_destruction? })
  end

  def has_other_role? deleted_role
    ! (roles.empty? or roles.all? {|role| role.id == deleted_role.id })
  end

  def give_user_access_to_group
    role = creator.try(:roles).try(:first)
    if role
      role.role_accesses.create(:accessable_id => self.id, :accessable_type => "Group")
    end
  end

  def check_preset_groups
    if name == configatron.default_group_name
      self.errors[:base] << "Cannot delete '#{configatron.default_group_name}' group."
      return false  
    # Alert: SUPER_USER
    elsif name == configatron.super_group_name  
      self.errors[:base] << "Cannot delete '#{configatron.super_group_name}' group."
      return false  
    end 
  end

  # make sure that the preset groups aren't changed
  def check_preset_groups_attributes
    # Alert: SUPER_USER
    if self.name_was == configatron.super_group_name
      self.name = configatron.super_group_name
    elsif self.name_was == configatron.default_group_name
      self.name = configatron.default_group_name
    end
  end

  #---------Super User---------#
  # Alert: SUPER_USER
  def is_super?
    name == configatron.super_group_name
  end

  def as_json(*args)
    #TODO: refactor convert to jbuilder
    hash = super(*args)
    hash.merge!({:roles=> self.roles})
    hash
  end
end

# == Schema Information
#
# Table name: groups
#
#  id         :integer          not null, primary key
#  name       :string(255)      default(""), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#