class ApplicationController < ActionController::Base
  before_filter :authenticate_user!
  protect_from_forgery

  # rescue_from CanCan::AccessDenied do |exception|
  #   redirect_to main_app.root_url, alert: exception.message
  # end

  def pagination_options(options = {})
    options[:page] = params[:page]
    options[:per_page] = params[:per_page]
    return options
  end
  
end
