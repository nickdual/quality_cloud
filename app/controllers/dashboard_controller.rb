class DashboardController < ApplicationController
	before_filter :selected_item
  
  # Authorization
  # authorize_resource class: false # Authorizing resource has no model
	
  # GET /
  def index
  	#@calls = Call.search(params)
    @calls = Call.today
  end

  private
  def selected_item
  	@selected = :dashboard
  end
end
