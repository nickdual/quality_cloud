class AssignedRolesController < ApplicationController
  load_and_authorize_resource
  # GET /groups
  # GET /groups.json
  def index
    @assigned_roles = if params[:role_id]
      AssignedRole.where(:role_id => params[:role_id])
    elsif params[:roleable_id] and params[:roleable_type]
      AssignedRole.where :roleable_id => params[:roleable_id], :roleable_type => params[:roleable_type]
    else
      AssignedRole.all
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @assigned_roles }
    end
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
    @assigned_role = AssignedRole.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @assigned_role }
    end
  end

  # GET /groups/new
  # GET /groups/new.json
  def new
    @assigned_role = AssignedRole.new

    respond_to do |format|
      format.html { render partial: 'form', layout: false }
      format.json { render json: @assigned_role }
    end
  end

  # GET /groups/1/edit
  def edit
    @assigned_role = AssignedRole.find(params[:id])
    @roles = Role.all()
    if @assigned_role.blank?
      "AssignedRole doesn't exist"
    else
      respond_to do |format|
        format.html { render partial: 'form', layout: false }
        format.json { render json: @assigned_role }
      end
    end
  end

  # POST /groups
  # POST /groups.json
  def create
    @assigned_role = AssignedRole.new(params[:assigned_role])

    respond_to do |format|
      if @assigned_role.save
        format.html { redirect_to @assigned_role, notice: 'AssignedRole was successfully created.' }
        format.json { render json: @assigned_role, status: :created }
      else
        format.html { render action: "new" }
        format.json { render json: @assigned_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /groups/1
  # PUT /groups/1.json
  def update
    @assigned_role = AssignedRole.find(params[:id])

    respond_to do |format|
      if @assigned_role.update_attributes(params[:group])
        @assigned_role.users.inheriters.update_all :role_id => @assigned_role.role_id
        format.html { redirect_to @assigned_role, notice: 'AssignedRole was successfully updated.' }
        format.json { render json: @assigned_role.to_json( :include =>  
            { :role => {:only => [:id, :name]} }) , status: :created }
      else
        format.html { render action: "edit" }
        format.json { render json: @assigned_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    @assigned_role = AssignedRole.find(params[:id])
    @assigned_role.destroy

    respond_to do |format|
      format.html { redirect_to groups_url }
      format.json { head :no_content }
    end
  end



end
