class QualityCloud.Views.RoleForm extends Backbone.View

  el: "#new_role_modal"
  events:
    "click #submit_new_form" : 'submitNewForm'
    "click #submit_edit_form" : 'submitEditForm'
    "click .close": "cleanForm"
  initialize:(options)=>
    @fetchForm(options?.model_id)

  fetchForm:(model_id)=>
    @isEditForm = (model_id?)
    url = if model_id? then Routes.edit_role_path(model_id) else Routes.new_role_path()
    callback = (@form) => @render()
    $.get url, {id: model_id}, callback, 'html'

  render:=>
    @$(".js-role-form").html(@form)
    @submitOnEnter()
    @$el.draggable
      handle: ".heading"

    @_showOverlay()

  _showOverlay:=>
    @overlay = $("<div id='black_overlay' class='black_overlay'></div>")
    @overlay.show()
    $("body").append(@overlay)
    @$el.css("z-index", @overlay.css("z-index")+4)

  _removeOverlay:=>
    @overlay.remove()


  submitNewForm:=>
    callback = (response) => 
      role = new QualityCloud.Models.Role(response)
      @collection.add(role)
      $('#select_role').append(JST['roles/role_record_dropdown'](role:role))
      $('#select_role').val(role.id)
      $('#select_role').trigger("change")
      @cleanForm()
    @submitForm(callback)

  submitEditForm:=>
    callback = (response) => 
      @collection.get(response.id).set(response)
      $("#select_role option[value=#{response.id}]").html(response.name)
      @cleanForm()
    @submitForm(callback)

  roleFormErrorsCallback:(errors)=>
    @$('.btn').removeClass('disabled')
    errorsString = ""
    errorsString += "<p class='text-error'>#{key} #{value}.</p>" for key, value of errors.responseJSON

    @$(".errors").html(errorsString)
    @$('.js-alert-errors').show()

  cleanForm:=>
    @$('.btn').removeClass('disabled')
    $(@el).hide()
    @$(".errors").html("")
    @$('.js-alert-errors').hide()
    @$("#role_name").val("")
    $("#role_supervisable, #role_disabled").attr("checked", false)
    @_removeOverlay()

  submitOnEnter:=>
    # disable submitting on enter key
    keyHandler = (e) => 
      if e.keyCode == 13
        if @isEditForm then @submitEditForm() else @submitNewForm()
        return false 
    @$('form input').bind("keypress", keyHandler)

  submitForm:(callback)=>
    @$('.btn').addClass('disabled')
    form = @$('form')
    options = success: callback, error: @roleFormErrorsCallback, dataType: 'json'
    form.ajaxSubmit(options); 

