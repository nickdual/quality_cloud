class QualityCloud.Views.GroupForm extends QualityCloud.Views.BaseView

  el: "#new_group_modal"
  events:
    "click #submit_new_form" : 'submitNewForm'
    "click #submit_edit_form" : 'submitEditForm'
    "click .close": "cleanForm"

  views_permissions:
    "assign_roles_group_tab": "#js_role_btns"


  initialize:(options={})=>
    @dispatcher = options.dispatcher
    @fetchForm(options?.model_id)

  fetchForm:(model_id)=>
    @isEditForm = (model_id?)
    url = if model_id? then Routes.edit_group_path(model_id) else Routes.new_group_path()
    $.get url, {id: model_id}, 
      (@form) => 
        @render()
      , 'html'

  render:=>
    @$(".js-group-form").html(@form)
    @submitOnEnter()
    @filterByPermission()
    @$el.draggable
      handle: ".heading"

    @_showOverlay()

  _showOverlay:=>
    @overlay = $("<div id='black_overlay' class='black_overlay'></div>")
    @overlay.show()
    $("body").append(@overlay)
    @$el.css("z-index", @overlay.css("z-index")+4)

  _removeOverlay:=>
    @overlay.remove()

  submitNewForm:=>
    callback = (response) => 
      group = new QualityCloud.Models.Group(response)
      @collection.add(group)
      $('#select_group').append(JST['groups/group_record_dropdown'](group:group))
      $('#select_group').val(group.id)
      $('#select_group').trigger("change")
      @dispatcher.trigger "renderGroup"
      @cleanForm()
    @submitForm(callback)

  submitEditForm:=>
    @submitForm (response) => 
      @collection.get(response.id).set(response)
      $("#select_group option[value=#{response.id}]").html(response.name)
      @dispatcher.trigger "renderGroup"
      @cleanForm()

  groupFormErrorsCallback:(errors)=>
    @$('.btn').removeClass('disabled')
    errorsString = ""
    errorsString += "<p class='text-error'>#{key} #{value}.</p>" for key, value of errors.responseJSON

    @$(".errors").html(errorsString)
    @$('.js-alert-errors').show()

  cleanForm:=>
    @$('.btn').removeClass('disabled')
    $(@el).hide()
    @$(".errors").html("")
    @$('.js-alert-errors').hide()
    @$("#group_name").val("")
    $("#checkboxes input[type='checkbox']").attr("checked", false)
    @_removeOverlay()

  submitOnEnter:=>
    # disable submitting on enter key
    keyHandler = (e) => 
      if e.keyCode == 13
        if @isEditForm then @submitEditForm() else @submitNewForm()
        return false 
    @$('form input').bind("keypress", keyHandler)

  submitForm:(callback)=>
    @$('.btn').addClass('disabled')
    form = @$('form')
    options = success: callback, error: @groupFormErrorsCallback, dataType: 'json'
    form.ajaxSubmit(options); 

