class QualityCloud.Views.PersonalTab extends QualityCloud.Views.BaseView

  el: "#personal_tab"

  views_permissions:
    "read_users":"#users_tab"
    "read_groups":"#groups_tab"
    "read_roles":"#roles_tab"

  initialize:=>
    @render()

  render:=>
    @filterByPermission()

  showUsersTab:()->
    if QualityCloud.current_user.has_permission "read_users"
      @$(".sub_menue").removeClass('active')
      @$("#users_tab").addClass('active')
      @$('.content').html(new QualityCloud.Views.UsersIndex().el)

  showGroupsTab:(e)=>
    if QualityCloud.current_user.has_permission "read_groups"
      @$(".sub_menue").removeClass('active')
      @$("#groups_tab").addClass('active')
      @$('.content').html(new QualityCloud.Views.GroupsIndex().el)

  showRolesTab:(e)=>
    if QualityCloud.current_user.has_permission "read_roles"
      @$(".sub_menue").removeClass('active')
      @$("#roles_tab").addClass('active')
      @$('.content').html(new QualityCloud.Views.RolesIndex().el)