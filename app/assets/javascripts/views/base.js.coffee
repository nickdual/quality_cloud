class QualityCloud.Views.BaseView extends Backbone.View
  
  filterByPermission:=>
    return unless @views_permissions
    for permission, selector of @views_permissions
      unless QualityCloud.current_user.has_permission permission
        @$(selector).remove()

  disabled_events:{}

  disableEvent:(e)=>
    return true if @disabled_events[e] #already disabled
    
    func_name = @events[e]
    return false unless func_name #events doesn't exist
    delete @events[e]

    @disabled_events[e] = func_name
    @delegateEvents()
    return true


  enableEvent:(e)=>
    return true if @events[e] #already disabled

    func_name = @disabled_events[e]
    return false unless func_name #events doesn't exist
    delete @disabled_events[e]

    @events[e] = func_name
    @delegateEvents()
    return true
