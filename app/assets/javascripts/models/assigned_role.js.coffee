class QualityCloud.Models.AssignedRole extends Backbone.Model
  urlRoot: '/assigned_roles'
  paramRoot: 'assigned_role'

class QualityCloud.Collections.AssignedRoles extends Backbone.Collection
  model: QualityCloud.Models.AssignedRole
  url:->
    return "/assigned_roles"