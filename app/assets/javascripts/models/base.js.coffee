class QualityCloud.Collections.BaseColection extends Backbone.Collection

  per_page_attr:{}
  initialize:(options = {})=>
    @data = {}
    if options.paginated is true
      Backbone.Pagination.enable this, 
        ipp:options.per_page||15
        page_attr: "page"
        ipp_attr : "per_page"


  fetch:(options={}, ignoreData = false)=>
    if !ignoreData and @data?
      options.data ||= {}
      options.data = $.extend(@data, options.data)
    super(options)

  reset:=>
    @resetPagination?()
    super()

  parse:(response, options)=>
    @_setPaginationAttrs(JSON.parse(options.xhr.getResponseHeader("X-Pagination")))
    super(response, options)

  _setPaginationAttrs:(pagination_options)=>
    @total_pages = pagination_options?.total_pages
    @total_entries = pagination_options?.total_entries
    @start_entry = pagination_options?.start_entry
    @end_entry = pagination_options?.end_entry
