class QualityCloud.Models.Call extends Backbone.Model
  urlRoot: 'calls'
  paramRoot: 'call'


class QualityCloud.Collections.Calls extends QualityCloud.Collections.BaseColection
  model: QualityCloud.Models.Call

  initialize:(options)=>
    pagination_attr = {per_page: QualityCloud.current_user.get("settings").calls_table_per_page}

    super($.extend(pagination_attr, options))
    QualityCloud.current_user.on "change:settings", @updatePagination

  updatePagination:=>
    calls_per_page = QualityCloud.current_user.get("settings").calls_table_per_page
    if @paginationConfig?.ipp? and calls_per_page and @paginationConfig?.ipp != calls_per_page
      @paginationConfig?.ipp = calls_per_page

  url:-> 
    return "calls/search"