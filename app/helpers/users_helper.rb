module UsersHelper
	def user_types_options
		User::TYPES.map{ |type| [type, type.titleize.gsub(/\s+/, '').constantize] }
	end

  def supervisors_list
    current_user.accessed_users.supervisables.map{ |user| [user.name, user.id] }
  end

  def groups_list
    current_user.accessed_groups.map{ |group| [group.name, group.id] }
  end
end
