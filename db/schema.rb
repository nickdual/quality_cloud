# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130824032121) do

  create_table "assigned_roles", :force => true do |t|
    t.integer  "role_id"
    t.integer  "roleable_id"
    t.string   "roleable_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "assigned_roles", ["roleable_id"], :name => "index_assigned_roles_on_roleable_id"
  add_index "assigned_roles", ["roleable_type"], :name => "index_assigned_roles_on_roleable_type"

  create_table "call_notes", :force => true do |t|
    t.integer  "call_id"
    t.integer  "user_id"
    t.string   "note"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "call_notes", ["call_id"], :name => "index_call_notes_on_call_id"
  add_index "call_notes", ["user_id"], :name => "index_call_notes_on_user_id"

  create_table "calls", :force => true do |t|
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "user_id"
    t.string   "call_type"
    t.string   "call_guid"
    t.string   "caller_id"
    t.string   "digits_dialed"
    t.string   "call_direction"
    t.integer  "call_duration"
    t.string   "dnis"
    t.string   "mp4_file"
    t.string   "audio_file"
    t.string   "agent_id"
    t.integer  "media_type"
  end

  create_table "groups", :force => true do |t|
    t.string   "name",       :default => "", :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "role_accesses", :force => true do |t|
    t.integer  "role_id"
    t.integer  "accessable_id"
    t.string   "accessable_type"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "role_accesses", ["accessable_type"], :name => "index_role_accesses_on_accessable_type"
  add_index "role_accesses", ["role_id"], :name => "index_role_accesses_on_role_id"

  create_table "roles", :force => true do |t|
    t.string   "name",         :default => "",    :null => false
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.text     "permissions"
    t.boolean  "supervisable"
    t.boolean  "disabled",     :default => false
  end

  create_table "saved_search_tabs", :force => true do |t|
    t.integer  "user_id"
    t.string   "search_params"
    t.string   "name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "",      :null => false
    t.string   "encrypted_password",     :default => "",      :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.string   "type",                   :default => "Agent"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "network_login"
    t.integer  "supervisor_id"
    t.string   "department"
    t.string   "phone"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "agent_id"
    t.integer  "group_id"
    t.boolean  "inherit_role"
    t.string   "full_name"
    t.text     "settings"
    t.boolean  "change_password",        :default => false
    t.boolean  "locked",                 :default => false
    t.boolean  "complex_password",       :default => false
    t.boolean  "super_user"
    t.boolean  "inherit_group_access",   :default => true
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
