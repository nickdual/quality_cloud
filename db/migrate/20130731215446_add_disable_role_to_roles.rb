class AddDisableRoleToRoles < ActiveRecord::Migration
  def change
    add_column :roles, :disabled, :boolean, :default => false
  end
end
