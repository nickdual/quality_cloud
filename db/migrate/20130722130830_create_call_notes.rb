class CreateCallNotes < ActiveRecord::Migration
  def change
    create_table :call_notes do |t|
      t.belongs_to :call
      t.belongs_to :user
      t.string :note

      t.timestamps
    end
    add_index :call_notes, :call_id
    add_index :call_notes, :user_id
  end
end
