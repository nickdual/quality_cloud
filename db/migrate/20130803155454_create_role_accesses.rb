class CreateRoleAccesses < ActiveRecord::Migration
  def change
    create_table :role_accesses do |t|
      t.integer :role_id
      t.integer :accessable_id
      t.string :accessable_type

      t.timestamps
    end
    add_index :role_accesses, :role_id
    add_index :role_accesses, :accessable_type
  end
end
