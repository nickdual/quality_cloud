class AddSupervisableToRoles < ActiveRecord::Migration
  def change
    add_column :roles, :supervisable, :boolean
  end
end
