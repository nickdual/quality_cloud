class ChangeUsersDepartmentType < ActiveRecord::Migration
  def self.up
    # rename_column :table, :old_column, :new_column

    change_table :users do |t|
      t.rename :department_id, :department
      t.change :department, :string
    end
  end
  def self.down
    change_table :tablename do |t|
      t.rename :department, :department_id
      t.change :department, :integer
    end
  end
end
