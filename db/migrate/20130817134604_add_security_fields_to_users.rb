class AddSecurityFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :change_password, :boolean, :default => false
    add_column :users, :locked, :boolean, :default => false
    add_column :users, :complex_password, :boolean, :default => false
  end
end
