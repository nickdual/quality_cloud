class AddAtributesToCalls < ActiveRecord::Migration
  def change
    add_column :calls, :user_id, :integer
    add_column :calls, :call_type, :integer
    add_column :calls, :call_guid, :string
    add_column :calls, :call_recording_url, :string
    add_column :calls, :caller_id, :string
    add_column :calls, :dialed_number, :string
    add_column :calls, :call_direction, :integer
    add_column :calls, :subscriber_id_1, :string
    add_column :calls, :subscriber_id_2, :string
    add_column :calls, :call_recording_time, :datetime
    add_column :calls, :call_duration, :integer
    add_column :calls, :dnis, :string
  end
end
