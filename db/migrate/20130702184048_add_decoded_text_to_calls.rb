class AddDecodedTextToCalls < ActiveRecord::Migration
  def change
    add_column :calls, :decoded_text, :text
  end
end
