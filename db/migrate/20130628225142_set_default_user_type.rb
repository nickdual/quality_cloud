class SetDefaultUserType < ActiveRecord::Migration
  def up
  	change_column_default :users, :type, 'Agent'
  end

  def down
  	change_column_default :users, :type, nil
  end
end
