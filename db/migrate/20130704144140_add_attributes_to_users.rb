class AddAttributesToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :network_login, :string
    add_column :users, :supervisor_id, :integer
    add_column :users, :department_id, :integer
    add_column :users, :phone, :string
    change_table :users do |t|
      t.attachment :avatar
    end
  end
  
  def self.down
    remove_column :users, :first_name
    remove_column :users, :last_name
    remove_column :users, :network_login
    remove_column :users, :supervisor_id
    remove_column :users, :department_id
    remove_column :users, :phone
    drop_attached_file :users, :avatar
  end
end
