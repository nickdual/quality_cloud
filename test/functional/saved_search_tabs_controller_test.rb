require 'test_helper'

class SavedSearchTabsControllerTest < ActionController::TestCase
  setup do
    @saved_search_tab = saved_search_tabs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:saved_search_tabs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create saved_search_tab" do
    assert_difference('SavedSearchTab.count') do
      post :create, saved_search_tab: { name: @saved_search_tab.name, search_params: @saved_search_tab.search_params, user_id: @saved_search_tab.user_id }
    end

    assert_redirected_to saved_search_tab_path(assigns(:saved_search_tab))
  end

  test "should show saved_search_tab" do
    get :show, id: @saved_search_tab
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @saved_search_tab
    assert_response :success
  end

  test "should update saved_search_tab" do
    put :update, id: @saved_search_tab, saved_search_tab: { name: @saved_search_tab.name, search_params: @saved_search_tab.search_params, user_id: @saved_search_tab.user_id }
    assert_redirected_to saved_search_tab_path(assigns(:saved_search_tab))
  end

  test "should destroy saved_search_tab" do
    assert_difference('SavedSearchTab.count', -1) do
      delete :destroy, id: @saved_search_tab
    end

    assert_redirected_to saved_search_tabs_path
  end
end
